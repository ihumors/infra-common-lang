package com.infra.common.req;

import io.swagger.annotations.ApiModelProperty;

/**
 * 分页查询场景，请求基类，主要用于扩展在请求参数对象上
 * e.g. extends BasePageReq
 *
 * @author PD
 */
public abstract class BasePageReq extends BaseRequest {
    /**
     * 当前页码
     */
    @ApiModelProperty(value = "当前页码")
    private int pageIndex = 1;
    /**
     * 每页展示条数
     */
    @ApiModelProperty(value = "每页展示条数")
    private int pageSize = 15;
    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private String sortField;
    /**
     * 排序类型 DESC:倒序,ASC:正序
     */
    @ApiModelProperty(value = "排序类型 DESC:倒序,ASC:正序")
    private String sortType = "DESC";

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }
}
