package com.infra.common.mybatis.plugins;

import java.util.Arrays;
import java.util.Set;

public class CommonUtils {
    public static void splitByComma(String original, Set<String> result) {
        if (original != null) {
            Arrays.stream(original.split(","))
                    .map(String::trim)
                    .filter(trimmed -> trimmed.length() > 0)
                    .forEach(result::add);
        }
    }
}
