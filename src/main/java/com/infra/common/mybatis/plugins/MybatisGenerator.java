package com.infra.common.mybatis.plugins;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * 应用官方mybatis-generator生成mybatis映射
 * mybatis-generator配置文件在/src/main/resources/generatorConfig-SupportDbCenter.xml
 *
 * @author PD
 */
public class MybatisGenerator {
    public static void main(String[] args) throws Exception {
        List<String> warnings = new ArrayList<String>();
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp
                .parseConfiguration(MybatisGenerator.class.getClassLoader().getResourceAsStream("generatorECologyConfig.xml")); //generatorTmU200Config,generatorLeanDeliveryConfig

        DefaultShellCallback shellCallback = new DefaultShellCallback(true);

        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, shellCallback, warnings);
        myBatisGenerator.generate(null);
        System.out.println("Mybatis generator runned successfully.");
        if (warnings.size() > 0) {
            System.out.println("\r\nWith warnings");
            for (String warning : warnings) {
                System.out.println(warning);
            }
        }

        System.out.println("\r\nRefresh your infrastructure project to see the latest db entity/mapper/mapperxml");
    }

}
