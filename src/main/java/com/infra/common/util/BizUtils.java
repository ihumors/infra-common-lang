package com.infra.common.util;

import com.alibaba.fastjson2.JSON;
import com.infra.common.resp.KeyValue;
import com.infra.common.resp.Page;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 业务工具类
 *
 * @author PD
 */
@SuppressWarnings("unchecked")
public class BizUtils {
    /**
     * build pageIndex;
     *
     * @param pageIndex 当前页
     * @param pageTotal 总页数
     * @return
     */
    public static int buildPageIndex(int pageIndex, int pageTotal) {
        if (pageIndex <= 1 || pageTotal == 0) {
            return 1;
        }

        if (pageIndex >= pageTotal) {
            return pageTotal;
        }

        return pageIndex;
    }

    /**
     * build page data object is not empty
     *
     * @param page
     */
    public static void buildPageData(Page page) {
        if (page.getQuery() == null) {
            page.setQuery(new HashMap<>());
        }
    }

    /**
     * build Order By Clause
     *
     * @param sortField fields
     * @param sortType  asc/desc
     */
    public static String buildOrderByClause(String sortField, String sortType) {
        return StringUtils.isEmpty(sortField) ? null : String.format("%s %s", sortField, sortType);
    }

    /**
     * 直接使用getResourceAsStream方法获取流
     * springboot项目中因为jar包中没有一个实际的路径存放文件
     *
     * @param fileName
     * @throws IOException
     */
    public void getFileContent(String fileName) throws IOException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fileName);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        StringBuffer stringBuffer = new StringBuffer();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuffer.append(line + "\n");
        }
        bufferedReader.close();
    }
    /**
     * 获取用户ID
     *
     * @param request request请求对象
     * @return
     */
    public static Long getUserId(HttpServletRequest request) {
        return ConvertUtils.parseLong(request.getHeader(AuthUtils.USER_ID));
    }

    /**
     * 获取用户名称
     *
     * @param request request请求对象
     * @return
     */
    public static String getUserName(HttpServletRequest request) {
        return request.getHeader(AuthUtils.USER_NAME);
    }

    /**
     * 获取激活状态
     *
     * @param request request请求对象
     * @return
     */
    public static Boolean isActive(HttpServletRequest request) {
        return ConvertUtils.parseBoolean(request.getHeader(AuthUtils.ACTIVE));
    }

    /**
     * 获取用户昵称
     *
     * @param request request请求对象
     * @return
     */
    public static String getNickName(HttpServletRequest request) {
        return request.getHeader(AuthUtils.NICK_NAME);
    }

    /**
     * 获取组织机构
     *
     * @param request request请求对象
     * @return
     */
    public static List<KeyValue> getOrgList(HttpServletRequest request) {
        String organization = request.getHeader(AuthUtils.ORGANIZATIONS);
        if (StringUtils.isNotBlank(organization)) {
            return JSON.parseArray(organization, KeyValue.class);
        }
        return new ArrayList<>();
    }

    /**
     * 获取角色列表
     *
     * @param request request请求对象
     * @return
     */
    public static List<KeyValue> getRoleList(HttpServletRequest request) {
        String roles = request.getHeader(AuthUtils.ROLES);
        if (StringUtils.isNotBlank(roles)) {
            return JSON.parseArray(roles, KeyValue.class);
        }
        return new ArrayList<>();
    }

    /**
     * 获取鉴权身份列表
     *
     * @param request request请求对象
     * @return
     */
    public static List<String> getAuthList(HttpServletRequest request) {
        String auths = request.getHeader(AuthUtils.AUTHS);
        if (StringUtils.isNotBlank(auths)) {
            return Arrays.asList(StringUtils.split(auths, ","));
        }
        return new ArrayList<>();
    }

    /**
     * 获取用户编号
     *
     * @param request 请求对象
     * @return
     */
    public static String getAuthToken(HttpServletRequest request) {
        return request.getHeader(AuthUtils.AUTHORIZATION);
    }

}
