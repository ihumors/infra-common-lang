package com.infra.common.util;


import org.apache.commons.lang3.time.DateUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 类型转换工具类
 *
 * @author PD
 */
public class ConvertUtils {
    public static final String PARSE_PATTERN_DATE = "yyyy-MM-dd";
    public static final String PARSE_PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";

    /**
     * 转日期
     *
     * @param input 输入值
     * @return
     */
    public static Date parseDate(Object input) {
        try {
            return input == null ? null : DateUtils.parseDate(input.toString(), ConvertUtils.PARSE_PATTERN_DATE);
        } catch (ParseException var2) {
            return null;
        }
    }

    /**
     * 转日期时间
     *
     * @param input 输入值
     * @return
     */
    public static Date parseDateTime(Object input) {
        try {
            return input == null ? null : DateUtils.parseDate(input.toString(), ConvertUtils.PARSE_PATTERN_DATETIME);
        } catch (ParseException var2) {
            return null;
        }
    }


    /**
     * 转字符串
     *
     * @param input 输入值
     * @return
     */
    public static String parseString(Object input) {
        return isNullOrEmpty(input) ? null : input.toString();
    }

    /**
     * 转布尔
     *
     * @param input 输入值
     * @return
     */
    public static Boolean parseBoolean(Object input) {
        return isNullOrEmpty(input) ? null : Boolean.valueOf(input.toString());
    }

    /**
     * 转整数
     *
     * @param input 输入值
     * @return
     */
    public static Integer parseInteger(Object input) {
        return isNullOrEmpty(input) ? null : Integer.valueOf(input.toString());
    }

    /**
     * 转布尔值
     *
     * @param input 输入值
     * @return
     */
    public static Long parseLong(Object input) {
        return isNullOrEmpty(input) ? null : Long.valueOf(input.toString());
    }

    /**
     * 转Byte
     *
     * @param input 输入值
     * @return
     */
    public static Byte parseByte(Object input) {
        return isNullOrEmpty(input) ? null : Byte.valueOf(input.toString());
    }

    /**
     * 实体对象转成Map
     *
     * @param obj 实体对象
     * @return
     */
    public static Map<String, Object> object2Map(Object obj) {
        Map<String, Object> map = new HashMap<>();
        if (obj == null) {
            return map;
        }
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                map.put(field.getName(), field.get(obj));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * Map转成实体对象
     *
     * @param map   实体对象包含属性
     * @param clazz 实体对象类型
     * @return
     */
    public static Object map2Object(Map<String, Object> map, Class<?> clazz) {
        if (map == null) {
            return null;
        }
        Object obj = null;
        try {
            obj = clazz.newInstance();

            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                    continue;
                }
                field.setAccessible(true);
                field.set(obj, map.get(field.getName()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 为空判断
     *
     * @param input 输入值
     * @return
     */
    public static boolean isNullOrEmpty(Object input) {
        if (input == null || "".equals(input.toString().trim()) || "null".equals(input)) {
            return true;
        }
        return false;
    }
}
