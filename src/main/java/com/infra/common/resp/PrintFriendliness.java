package com.infra.common.resp;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * 自身内容能以可读方式输出
 *
 * @author PD
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class PrintFriendliness implements Serializable {

    private static final long serialVersionUID = -235822080790001901L;

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ":"
                + JSON.toJSONString(this, JSONWriter.Feature.PrettyFormat, JSONWriter.Feature.IgnoreNoneSerializable);
    }

}
