package com.infra.common.resp;

import com.github.pagehelper.PageInfo;
import com.infra.common.req.BasePageReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页对象
 *
 * @author PD
 */
@SuppressWarnings("unchecked")
@ApiModel(value = "PageEx", description = "分页查询数据视图对象<列表数据报文对象，请求参数报文对象>")
public class PageEx<T, V extends BasePageReq> extends PrintFriendliness {
    /**
     * 总页数
     */
    @ApiModelProperty(value = "页码总数")
    private int pageTotal = -1;
    /**
     * 总条数
     */
    @ApiModelProperty(value = "总条数")
    private long total = -1;
    /**
     * 数据集合
     */
    @ApiModelProperty(value = "数据集合")
    private List<T> pageList;
    /**
     * 请求参数报文对象
     */
    @ApiModelProperty(value = "查询参数,BasePageReq子类对象")
    private V query;

    public int getPageTotal() {
        return (int) Math.ceil((double) this.getTotal() / query.getPageSize());
    }

    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getPageList() {
        return pageList;
    }

    public void setPageList(List<T> pageList) {
        this.pageList = pageList;
    }

    public V getQuery() {
        return query;
    }

    public void setQuery(V query) {
        this.query = query;
    }

    public PageEx() {

    }

    public PageEx(List<T> list, Long total, V req) {
        this.setTotal(total);
        this.setPageList(list);
        this.setQuery(req);
    }

    /**
     * 构建PageEx对象
     *
     * @param total 总数
     * @param req   请求对象
     * @param <T>   数据项类型
     * @param <V>   请求参数类型
     * @return
     */
    public static <T, V extends BasePageReq> PageEx<T, V> builder(Long total, V req) {
        return new PageEx(new ArrayList(), total, req);
    }

    /**
     * 构建PageEx对象
     *
     * @param list 继承了ArrayList的PageHelper的Page列表对象
     * @param req  请求对象
     * @param <T>  数据项泛型
     * @param <V>  请求参数泛型
     * @return
     */
    public static <T, V extends BasePageReq> PageEx<T, V> builder(List<T> list, V req) {
        PageInfo<T> pageInfo = new PageInfo<>(list);
        return new PageEx(pageInfo.getList(), pageInfo.getTotal(), req);
    }

    /**
     * 构建PageEx对象
     *
     * @param pageInfo PageInfo任意类型对象
     * @param req      请求对象
     * @param <T>      数据项泛型
     * @param <V>      请求参数泛型
     * @return
     */
    public static <T, V extends BasePageReq> PageEx<T, V> builder(PageInfo<?> pageInfo, V req) {
        return new PageEx(pageInfo.getList(), pageInfo.getTotal(), req);
    }

    @Deprecated
    public static <T, V extends BasePageReq> PageEx<T, V> buildTotal(List<?> list, V req) {
        PageInfo<?> pageInfo = new PageInfo<>(list);
        return new PageEx<>(new ArrayList<>(), pageInfo.getTotal(), req);
    }

    @Deprecated
    public static <T, V extends BasePageReq> PageEx<T, V> buildTotal(PageInfo<?> pageInfo, V req) {
        return new PageEx<>(new ArrayList<>(), pageInfo.getTotal(), req);
    }
}
