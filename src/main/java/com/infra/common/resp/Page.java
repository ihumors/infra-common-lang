package com.infra.common.resp;

import com.github.pagehelper.PageInfo;
import com.infra.common.req.BasePageReq;
import com.infra.common.util.ConvertUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页对象
 *
 * @author PD
 */
@SuppressWarnings("unchecked")
@ApiModel(value = "Page", description = "分页数据传输对象<列表数据对象>")
public class Page<T> extends BasePageReq {
    /**
     * 总页数
     */
    @ApiModelProperty(value = "页码总数")
    private int pageTotal = -1;
    /**
     * 总条数
     */
    @ApiModelProperty(value = "总条数")
    private long total = -1;
    /**
     * 数据集合
     */
    @ApiModelProperty(value = "数据集合")
    private List<T> pageList;
    /**
     * 查询参数集合，用于存放查询的参数信息
     */
    @ApiModelProperty(value = "查询参数,Map集合")
    private Map<String, Object> query = new HashMap<>();

    public int getPageTotal() {
        return (int) Math.ceil((double) this.getTotal() / this.getPageSize());
    }

    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getPageList() {
        return pageList;
    }

    public void setPageList(List<T> pageList) {
        this.pageList = pageList;
    }

    public Page() {

    }

    public Map<String, Object> getQuery() {
        return query;
    }

    public void setQuery(Map<String, Object> query) {
        this.query = query;
    }

    /**
     * 构建 Page 对象
     *
     * @param list 继承了ArrayList的PageHelper的Page列表对象
     * @return
     */
    public Page<T> build(List<T> list) {
        return build(new PageInfo<>(list));
    }

    /**
     * 构建 Page 对象
     *
     * @param pageInfo PageInfo对象
     * @return
     */
    public Page<T> build(PageInfo<T> pageInfo) {
        this.setTotal(pageInfo.getTotal());
        this.setPageList(pageInfo.getList());
        return this;
    }

    /**
     * 构建全新的 Page 对象
     *
     * @param list 继承了ArrayList的PageHelper的Page列表对象
     * @param req  继承了BasePageReq的查询对象
     * @return
     */
    public static <T> Page builder(List<T> list, BasePageReq req) {
        return builder(new PageInfo<>(list), req);
    }

    /**
     * 构建全新的 Page 对象，list为空数组
     *
     * @param total 总条数
     * @param req
     * @param <T>
     * @return
     */
    public static <T> Page builder(Long total, BasePageReq req) {
        Page<T> page = new Page();
        page.setPageSize(req.getPageSize());
        page.setPageIndex(req.getPageIndex());
        page.setSortField(req.getSortField());
        page.setSortType(req.getSortType());
        page.setQuery(ConvertUtils.object2Map(req));
        page.setTotal(total);
        page.setPageList(new ArrayList<>());

        return page;
    }

    /**
     * 构建全新的 Page 对象，list为pageInfo.getList()
     *
     * @param pageInfo PageInfo对象
     * @param req      继承了BasePageReq的查询对象
     */
    public static <T> Page builder(PageInfo<T> pageInfo, BasePageReq req) {
        final Page page = builder(pageInfo.getTotal(), req);
        page.getPageList().addAll(pageInfo.getList());
        return page;
    }

}
