package com.infra.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * 键值对对象
 *
 * @author PD
 */
@SuppressWarnings("serial")
@ApiModel(value = "KeyValue", description = "键值对对象")
public class KeyValue extends PrintFriendliness {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "text")
    private String text;

    public KeyValue() {

    }

    public KeyValue(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyValue keyValue = (KeyValue) o;
        return Objects.equals(id, keyValue.id) &&
                Objects.equals(text, keyValue.text);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, text);
    }
}