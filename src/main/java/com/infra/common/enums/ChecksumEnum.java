package com.infra.common.enums;

import com.infra.common.resp.KeyValue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 对称加密算法枚举
 *
 * @author PD
 */
public enum ChecksumEnum implements BaseEnum {
    MD5(1, "MD5"),
    SHA1(2, "SHA-1"),
    SHA256(3, "SHA-256"),
    SHA384(4, "SHA-384"),
    SHA512(5, "SHA-512");

    /**
     * 状态码
     */
    private int code;

    /**
     * 状态码描述
     */
    private String message;

    ChecksumEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    /**
     * 通过码获取枚举项
     *
     * @param code
     * @return
     */
    public static ChecksumEnum getByCode(int code) {
        return Arrays.stream(ChecksumEnum.values())
                .filter(item -> item.getCode() == code)
                .findFirst()
                .orElse(ChecksumEnum.MD5);
    }

    /**
     * 枚举项值转为List
     * e.g. ChecksumEnum.toList();
     *
     * @return
     */
    public static List<KeyValue> toList() {
        return Arrays.stream(ChecksumEnum.values())
                .map(item -> new KeyValue(String.valueOf(item.getCode()), item.getMessage()))
                .collect(Collectors.toList());
    }

}