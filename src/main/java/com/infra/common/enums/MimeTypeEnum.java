package com.infra.common.enums;

import com.infra.common.resp.KeyValue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 媒体资源类型枚举
 *
 * @author PD
 */
public enum MimeTypeEnum implements BaseEnum {
    UNKNOWN(0, "", "UNKNOWN"),
    VIDEO_3GP(1, "3gp", "video/3gpp"),
    APPLICATION_APK(2, "apk", "application/vnd.android.package-archive"),
    VIDEO_ASF(3, "asf", "video/x-ms-asf"),
    VIDEO_AVI(4, "avi", "video/x-msvideo"),
    APPLICATION_BIN(5, "bin", "application/octet-stream"),
    IMAGE_BMP(6, "bmp", "image/bmp"),
    TEXT_C(7, "c", "text/plain"),
    APPLICATION_CLASS(8, "class", "application/octet-stream"),
    TEXT_CONF(9, "conf", "text/plain"),
    TEXT_CPP(10, "cpp", "text/plain"),
    APPLICATION_DOC(11, "doc", "application/msword"),
    APPLICATION_DOCX(12, "docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    APPLICATION_XLS(13, "xls", "application/vnd.ms-excel"),
    APPLICATION_XLSX(14, "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    APPLICATION_EXE(15, "exe", "application/octet-stream"),
    IMAGE_GIF(16, "gif", "image/gif"),
    APPLICATION_GTAR(17, "gtar", "application/x-gtar"),
    APPLICATION_GZ(18, "gz", "application/x-gzip"),
    TEXT_H(19, "h", "text/plain"),
    TEXT_HTM(20, "htm", "text/html"),
    TEXT_HTML(21, "html", "text/html"),
    APPLICATION_JAR(22, "jar", "application/java-archive"),
    TEXT_JAVA(23, "java", "text/plain"),
    IMAGE_JPEG(24, "jpeg", "image/jpeg"),
    IMAGE_JPG(25, "jpg", "image/jpeg"),
    APPLICATION_JS(26, "js", "application/x-javascript"),
    TEXT_LOG(27, "log", "text/plain"),
    AUDIO_M3U(28, "m3u", "audio/x-mpegurl"),
    AUDIO_M4A(29, "m4a", "audio/mp4a-latm"),
    AUDIO_M4B(30, "m4b", "audio/mp4a-latm"),
    AUDIO_M4P(31, "m4p", "audio/mp4a-latm"),
    AUDIO_M4U(32, "m4u", "video/vnd.mpegurl"),
    AUDIO_M4V(33, "m4v", "video/x-m4v"),
    AUDIO_MOV(34, "mov", "video/quicktime"),
    AUDIO_MP2(35, "mp2", "audio/x-mpeg"),
    AUDIO_MP3(36, "mp3", "audio/x-mpeg"),
    VIDEO_MP4(37, "mp4", "video/mp4"),
    APPLICATION_MPC(38, "mpc", "application/vnd.mpohun.certificate"),
    VIDEO_MPE(39, "mpe", "video/mpeg"),
    VIDEO_MPEG(40, "mpeg", "video/mpeg"),
    VIDEO_MPG(41, "mpg", "video/mpeg"),
    VIDEO_MPG4(42, "mpg4", "video/mp4"),
    AUDIO_MPEG(43, "mpga", "audio/mpeg"),
    APPLICATION_MSG(44, "msg", "application/vnd.ms-outlook"),
    AUDIO_OGG(45, "ogg", "audio/ogg"),
    APPLICATION_PDF(46, "pdf", "application/pdf"),
    IMAGE_PNG(47, "png", "image/png"),
    APPLICATION_PPS(48, "pps", "application/vnd.ms-powerpoint"),
    APPLICATION_PPT(49, "ppt", "application/vnd.ms-powerpoint"),
    APPLICATION_PPTX(50, "pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"),
    TEXT_PROP(51, "prop", "text/plain"),
    TEXT_RC(52, "rc", "text/plain"),
    AUDIO_RMVB(53, "rmvb", "audio/x-pn-realaudio"),
    APPLICATION_RTF(54, "rtf", "application/rtf"),
    TEXT_SH(55, "sh", "text/plain"),
    APPLICATION_TAR(56, "tar", "application/x-tar"),
    APPLICATION_TGZ(57, "tgz", "application/x-compressed"),
    TEXT_TXT(58, "txt", "text/plain"),
    AUDIO_WAV(59, "wav", "audio/x-wav"),
    AUDIO_WMA(60, "wma", "audio/x-ms-wma"),
    AUDIO_WMV(61, "wmv", "audio/x-ms-wmv"),
    APPLICATION_WPS(62, "wps", "application/vnd.ms-works"),
    TEXT_XML(63, "xml", "text/plain"),
    APPLICATION_ZS(64, "z", "application/x-compress"),
    APPLICATION_ZIP(65, "zip", "application/x-zip-compressed");

    /**
     * 状态码
     */
    private int code;

    /**
     * 状态码描述
     */
    private String message;

    private String value;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getValue() {
        return value;
    }

    MimeTypeEnum(int code, String value, String message) {
        this.code = code;
        this.message = message;
        this.value = value;
    }

    /**
     * 通过码获取枚举项
     *
     * @param value
     * @return
     */
    public static MimeTypeEnum getByCode(int value) {
        return Arrays.stream(MimeTypeEnum.values())
                .filter(item -> item.getCode() == value)
                .findFirst()
                .orElse(MimeTypeEnum.UNKNOWN);
    }

    /**
     * 通过值获取枚举项
     *
     * @param value
     * @return
     */
    public static MimeTypeEnum getByValue(String value) {
        return Arrays.stream(MimeTypeEnum.values())
                .filter(item -> item.getValue().equals(value))
                .findFirst()
                .orElse(MimeTypeEnum.UNKNOWN);
    }

    /**
     * 通过MimeType全称获取枚举项
     *
     * @param value
     * @return
     */
    public static MimeTypeEnum getByMessage(String value) {
        return Arrays.stream(MimeTypeEnum.values())
                .filter(item -> item.getMessage().equals(value))
                .findFirst()
                .orElse(MimeTypeEnum.UNKNOWN);
    }

    /**
     * 枚举项值转为List
     * e.g. ErrorCodeEnum.toList();
     *
     * @return
     */
    public static List<KeyValue> toList() {
        return Arrays.stream(MimeTypeEnum.values())
                .map(item -> new KeyValue(String.valueOf(item.getCode()), item.getValue()))
                .collect(Collectors.toList());
    }
}
