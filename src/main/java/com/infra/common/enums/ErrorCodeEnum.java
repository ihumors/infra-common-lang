package com.infra.common.enums;

import com.infra.common.resp.KeyValue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 错误编码枚举
 *
 * @author PD
 */
public enum ErrorCodeEnum implements BaseEnum {

    FAIL(198, "请求处理失败"),
    NONE(0, "无异常"),
    FORBIDDEN(403, "Forbidden"),
    UNAUTHORIZED(401, "未授权"),
    NOT_ACTIVATED(406, "未激活"),
    CONFLICT(409, "在另端登陆"),
    LOCKED(423, "已锁定"),
    ACCOUNT_NOT_EXISTS(461, "账号不存在"),
    PASSWORD_ERROR(462, "账号密码错误"),
    CANCEL(463, "已注销"),
    VALIDATE_CODE_ERROR(470, "验证码错误");


    private int code;

    private String message;

    ErrorCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    /**
     * 通过错误码获取枚举项
     *
     * @param code
     * @return
     */
    public static ErrorCodeEnum getByCode(int code) {
        return Arrays.stream(ErrorCodeEnum.values())
                .filter(item -> item.getCode() == code)
                .findFirst().orElse(NONE);
    }

    /**
     * 枚举项值转为List
     * e.g. ErrorCodeEnum.toList();
     *
     * @return
     */
    public static List<KeyValue> toList() {
        return Arrays.stream(ErrorCodeEnum.values())
                .map(item -> new KeyValue(String.valueOf(item.getCode()), item.getMessage()))
                .collect(Collectors.toList());
    }
}

