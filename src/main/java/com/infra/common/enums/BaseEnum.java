package com.infra.common.enums;

/**
 * 枚举声明基类
 *
 * @author PD
 */
public interface BaseEnum {
    int getCode();

    String getMessage();
}
