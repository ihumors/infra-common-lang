package com.infra;

import com.infra.common.util.ThreadPoolUtils;

public class ThreadPoolUtilsTests {
    public static void main(String[] args) {
        String taskResult = ThreadPoolUtils.getTaskResult(ThreadPoolUtils.submit(() -> {
            System.out.println("CACHED_THREAD_POOL submit");
            return "TaskResult";
        }), "失败了");
        System.out.println(taskResult);

        ThreadPoolUtils.execute(() -> System.out.println("DEFAULT_FIXED_THREAD_POOL execute"));
    }
}
