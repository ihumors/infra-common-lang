# common-lang

公共类设计库作为基础设施库JAR包供所有应用项目使用
```xml
<dependency>
    <groupId>com.infra</groupId>
    <artifactId>infra-common-lang</artifactId>
    <version>{infra-common-lang.version}</version>
</dependency>
```
##版本
版本 1.0.0

